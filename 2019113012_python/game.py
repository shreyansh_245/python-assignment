import pygame
import math
from config import *
import time
arrx1 = [50, 170, 320, 450, 750]  # arrays storing x coordinates
arrx2 = [80, 180, 380, 500, 690]
arrx3 = [50, 170, 280, 550, 700]
arrx4 = [50, 250, 450, 550, 750]
arrx5 = [50, 150, 250, 350, 450]
arrx6 = [70, 150, 220, 550, 700]
arry = [60, 185, 350, 490, 637, 650]  # array storing y coordinates
fixedobsx = [120, 140, 160, 205, 900, 399, 555, 500, 270, 110, 700, 900]
fixedobsy = [710, 570, 440, 300, 150, 5]
scorey = [610, 550, 460, 400, 320, 250, 160, 100, 20]
scorerev = [20, 100, 160, 250, 320, 400, 460, 550, 610]
fixedcollid = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
pygame.display.set_caption("First Game")
distance = []
green = (0, 255, 0)  # color variables
brown = (150, 75, 0)
x = 540
y = 720
width = 16
height = 16
vel = 10
jump = 0
jumpCount = 10
run = 1
xn = 540
yn = 190
i = 0
flag = [0, 0, 0, 0, 0]
flag1 = 1
flag2 = 0
objvar = 0
start_ticks = pygame.time.get_ticks()  # variables to store objects
player1 = pygame.image.load("spaceship1.png")
player1 = pygame.transform.scale(player1, (32, 32))
player2 = pygame.image.load("spaceshipinvert.png")
player2 = pygame.transform.scale(player2, (32, 32))
fixedobj = pygame.image.load("tree.png")
fixedobj = pygame.transform.scale(fixedobj, (32, 32))
enemy1 = pygame.image.load("spaceship1.png")
enemy1 = pygame.transform.scale(enemy1, (64, 64))
shark = pygame.image.load("shark.gif")
shark = pygame.transform.scale(shark, (64, 64))
arr2 = [enemy1, enemy1, enemy1, enemy1, enemy1]
c1 = 1
c2 = 1
pinit1 = 1
pinit2 = 0
flagmessage = 0
messageplayer1 = 9
messageplayer2 = 0
messageplayer1end = 0
messageplayer2end = 0


def change(minim):
    if minim > 700:
        minim = 0
    if minim < 20:
        minim = 800


def changepos(x, y):
    if y < 20:
        y = 0
        x = 0
    if y > 700:
        y = 720
        x = 540


pygame.display.update()


def blit(k, l, z):   # function to blit obstacles
    win.blit(z, (k, l))


def blitfix(k, l):  # function to blit fixed obstacles
    win.blit(fixedobj, (k, l))


def iscollision(enex, px, eney, py):
    distance1 = math.sqrt((math.pow(enex - px, 2)) + (math.pow(eney - py, 2)))
    if distance1 < 40:
        return True
    else:
        return False


def redrawwindow():  # function to update game screen
    win.fill((0, 0, 255))
    pygame.draw.line(win, green, (0, 0), (1080, 0), 70)
    pygame.draw.line(win, brown, (0, 150), (1080, 150), 64)
    pygame.draw.line(win, brown, (0, 300), (1080, 300), 64)
    pygame.draw.line(win, brown, (0, 450), (1080, 450), 64)
    pygame.draw.line(win, brown, (0, 600), (1080, 600), 64)
    pygame.draw.line(win, green, (0, 780), (1080, 780), 130)
    if pinit1 == 1:
        messageup("start", brown)
        messagedown("end", brown)
        win.blit(player1, (x, y))
    if pinit2 == 1:
        messageup("end", brown)
        messagedown("start", brown)
        win.blit(player2, (x, y))
    pygame.display.update()


score = 0  # score variable
minim = 720
start = 0
end = 0
total = 0
level = 1
score2 = 0
seconds1 = 0
seconds2 = 0
while run:  # main game loop
    if flagmessage != 0:
        win.fill(brown)
        if messageplayer1 == 1:
            errormessage("Collision, player2 wins", green)
        if messageplayer2 == 1:
            errormessage("Collision, player1 wins", green)
        if messageplayer2end == 1:
            print(seconds1, seconds2)
            if seconds1 < seconds2:
                errormessage("Player1 wins", green)
            else:
                errormessage("Player2 wins", green)
        if flagmessage == 1:
            seconds2 = 0
            seconds1 = 0
        flagmessage -= 1
        pygame.display.update()

    if flagmessage == 0:
        if flag1 == 1:
            start = time.time() / 1000
            seconds1 = (pygame.time.get_ticks() - start_ticks) / 1000
            pygame.time.delay(100)
            tree = 0
            tree1 = 0
            for event in pygame.event.get():
                if event.type == pygame.QUIT:  # condition to quit the game
                    run = 0
            keys = pygame.key.get_pressed()

            if keys[pygame.K_LEFT] and x > vel - 2:  # button keys
                x = x - vel
            if keys[pygame.K_RIGHT] and x < 1080 - 64:
                x = x + vel
            if keys[pygame.K_UP] and y > vel - 2:
                y = y - vel
            if keys[pygame.K_DOWN] and y < 780 - 64 - vel + 10:
                y = y + vel
            redrawwindow()
            if c1 == 1:  # condition to change message display variables
                x = 540
                y = 720
                pinit1 = 1
                pinit2 = 0
                c1 = c1 - 1
                minim = 800
                messageplayer1 = 0
                messageplayer1end = 0
            blitfix(fixedobsx[0], fixedobsy[0])  # blit fixed objects
            blitfix(fixedobsx[11], fixedobsy[0])
            blitfix(fixedobsx[1], fixedobsy[1])
            blitfix(fixedobsx[10], fixedobsy[1])
            blitfix(fixedobsx[2], fixedobsy[2])
            blitfix(fixedobsx[9], fixedobsy[2])
            blitfix(fixedobsx[3], fixedobsy[3])
            blitfix(fixedobsx[8], fixedobsy[3])
            blitfix(fixedobsx[4], fixedobsy[4])
            blitfix(fixedobsx[7], fixedobsy[4])
            blitfix(fixedobsx[5], fixedobsy[5])
            blitfix(fixedobsx[3], fixedobsy[5])
            fixedcollid[0] = (iscollision(x, fixedobsx[0], y, fixedobsy[0]))
            fixedcollid[1] = (iscollision(x, fixedobsx[11], y, fixedobsy[0]))
            fixedcollid[2] = (iscollision(x, fixedobsx[1], y, fixedobsy[1]))
            fixedcollid[3] = (iscollision(x, fixedobsx[10], y, fixedobsy[1]))
            fixedcollid[4] = (iscollision(x, fixedobsx[2], y, fixedobsy[2]))
            fixedcollid[5] = (iscollision(x, fixedobsx[9], y, fixedobsy[2]))
            fixedcollid[6] = (iscollision(x, fixedobsx[3], y, fixedobsy[3]))
            fixedcollid[7] = (iscollision(x, fixedobsx[8], y, fixedobsy[3]))
            fixedcollid[8] = (iscollision(x, fixedobsx[4], y, fixedobsy[4]))
            fixedcollid[9] = (iscollision(x, fixedobsx[7], y, fixedobsy[4]))
            fixedcollid[10] = (iscollision(x, fixedobsx[5], y, fixedobsy[5]))
            fixedcollid[11] = (iscollision(x, fixedobsx[3], y, fixedobsy[5]))
            objvar = fixedcollid[0]
            for i in range(1, 12):  # checking if collision occurred
                objvar = objvar or fixedcollid[i]
                if objvar == 1:
                    tree = 1
            for t in range(0, 5):  # initializing moving obstacles
                arrx1[t] = arrx1[t] + obstacspeed
                if arrx1[t] > 1080:
                    arrx1[t] = 0
                blit(arrx1[t], arry[0], enemy1)
            for t in range(0, 5):
                arrx2[4 - t] = arrx2[4 - t] - obstacspeed
                if arrx2[t] < 0:
                    arrx2[t] = 1080
                blit(arrx2[t], arry[1], shark)
            for t in range(0, 5):
                arrx3[t] = arrx3[t] + obstacspeed
                if arrx3[t] > 1080:
                    arrx3[t] = 0
                blit(arrx3[t], arry[2], enemy1)
            for t in range(0, 5):
                arrx4[4 - t] = arrx4[4 - t] - obstacspeed
                if arrx4[t] < 0:
                    arrx4[t] = 1080
                blit(arrx4[t], arry[3], shark)
            for t in range(0, 5):
                arrx5[t] = arrx5[t] + obstacspeed
                if arrx5[t] > 1080:
                    arrx5[t] = 0
                blit(arrx5[t], arry[4], enemy1)
                k = 0
            for i in range(0, k + 50):
                distance.append(0)
            for k in range(0, 5):  # storing distance positions in array
                distance[k] = (iscollision(x, arrx1[k], y, arry[0]))
                distance[k + 5] = (iscollision(x, arrx2[k], y, arry[1]))
                distance[k + 10] = (iscollision(x, arrx3[k], y, arry[2]))
                distance[k + 15] = (iscollision(x, arrx4[k], y, arry[3]))
                distance[k + 20] = (iscollision(x, arrx5[k], y, arry[4]))
            var = distance[0]
            for i in range(0, 30):
                var = var or distance[i]
                if var == 1:
                    tree1 = 1
            if y < 630 and y > scorey[0] and minim > y:
                score = score + 10
                minim = scorey[0]
            if y > scorey[0] and y < scorey[1] and minim > y:
                score = score + 10
                minim = scorey[1]
            if y < scorey[1] and y > scorey[2] and minim > y:
                score = score + 5
                minim = scorey[2]
            if y < scorey[2] and y > scorey[3] and minim > y:
                score = score + 10
                minim = scorey[3]
            if y < scorey[3] and y > scorey[4] and minim > y:
                score = score + 5
                minim = scorey[4]
            if y < scorey[4] and y > scorey[5] and minim > y:
                score = score + 10
                minim = scorey[5]
            if y < scorey[5] and y > scorey[6] and minim > y:
                score = score + 5
                minim = scorey[6]
            if y < scorey[6] and y > scorey[7] and minim > y:
                score = score + 10
                minim = scorey[7]
            if y < scorey[7] and y > scorey[8] and minim > y:
                score = score + 5
                minim = scorey[8]
            if y < scorey[8] and y > 0 and minim > y:
                score = score + 10
                end = time.time()
                minim = 0
                flag1 = 0
                flag2 = 1
                c2 = 1
                score2 = 0
                pygame.display.update()
                print("Reached")

            text1 = font.render('score: ' + str(score), 1, (0, 0, 0))
            text2 = font.render('score2: ' + str(score2), 1, (0, 0, 0))
            text3 = font.render('level: ' + str(level), 1, (0, 0, 0))
            printf1(text1)
            printf2(text2)
            printf3(text3)
            if tree == 1 or tree1 == 1:
                flag1 = 1
                flag2 = 0
                c1 = 1
                obstacspeed = 40
                level = 1
                score = 0
                score2 = 0
                messageplayer1 = 1
                flagmessage = 1000
                seconds1 = 0
                seconds2 = 0
                obstacspeed = 20
        if flag2 == 1:  # Same conditions to second part of the loop
            # if (c1==1):
            #     change(minim)

            #    c1=c1-1
            tree2 = 0
            tree3 = 0
            if (c2 == 1):
                x = 540
                y = 5
                pinit1 = 0
                pinit2 = 1
                c2 = c2 - 1
                minim = 0
                messageplayer2 = 0
                messageplayer2end = 0
            start = time.time() / 1000
            seconds2 = (pygame.time.get_ticks() - start_ticks) / 1000
            pygame.time.delay(100)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    run = 0
            keys = pygame.key.get_pressed()
            if keys[pygame.K_d] and x > vel - 2:
                x = x - vel
            if keys[pygame.K_a] and x < 1080 - 64:
                x = x + vel
            if keys[pygame.K_s] and y > vel - 2:
                y = y - vel
            if keys[pygame.K_w] and y < 780 - 64 - vel + 10:
                y = y + vel
            redrawwindow()
            blitfix(fixedobsx[0], fixedobsy[0])
            blitfix(fixedobsx[11], fixedobsy[0])
            blitfix(fixedobsx[1], fixedobsy[1])
            blitfix(fixedobsx[10], fixedobsy[1])
            blitfix(fixedobsx[2], fixedobsy[2])
            blitfix(fixedobsx[9], fixedobsy[2])
            blitfix(fixedobsx[3], fixedobsy[3])
            blitfix(fixedobsx[8], fixedobsy[3])
            blitfix(fixedobsx[4], fixedobsy[4])
            blitfix(fixedobsx[7], fixedobsy[4])
            blitfix(fixedobsx[5], fixedobsy[5])
            blitfix(fixedobsx[3], fixedobsy[5])
            fixedcollid[0] = (iscollision(x, fixedobsx[0], y, fixedobsy[0]))
            fixedcollid[1] = (iscollision(x, fixedobsx[11], y, fixedobsy[0]))
            fixedcollid[2] = (iscollision(x, fixedobsx[1], y, fixedobsy[1]))
            fixedcollid[3] = (iscollision(x, fixedobsx[10], y, fixedobsy[1]))
            fixedcollid[4] = (iscollision(x, fixedobsx[2], y, fixedobsy[2]))
            fixedcollid[5] = (iscollision(x, fixedobsx[9], y, fixedobsy[2]))
            fixedcollid[6] = (iscollision(x, fixedobsx[3], y, fixedobsy[3]))
            fixedcollid[7] = (iscollision(x, fixedobsx[8], y, fixedobsy[3]))
            fixedcollid[8] = (iscollision(x, fixedobsx[4], y, fixedobsy[4]))
            fixedcollid[9] = (iscollision(x, fixedobsx[7], y, fixedobsy[4]))
            fixedcollid[10] = (iscollision(x, fixedobsx[5], y, fixedobsy[5]))
            fixedcollid[11] = (iscollision(x, fixedobsx[3], y, fixedobsy[5]))
            objvar = fixedcollid[0]
            for i in range(1, 12):
                objvar = objvar or fixedcollid[i]
                if objvar == 1:
                    tree3 = 1
            for t in range(0, 5):
                arrx1[t] = arrx1[t] + obstacspeed
                if arrx1[t] > 1080:
                    arrx1[t] = 0
                blit(arrx1[t], arry[0], enemy1)
            for t in range(0, 5):
                arrx2[4 - t] = arrx2[4 - t] - obstacspeed
                if arrx2[t] < 0:
                    arrx2[t] = 1080
                blit(arrx2[t], arry[1], shark)
            for t in range(0, 5):
                arrx3[t] = arrx3[t] + obstacspeed
                if arrx3[t] > 1080:
                    arrx3[t] = 0
                blit(arrx3[t], arry[2], enemy1)
            for t in range(0, 5):
                arrx4[4 - t] = arrx4[4 - t] - obstacspeed
                if arrx4[t] < 0:
                    arrx4[t] = 1080
                blit(arrx4[t], arry[3], shark)
            for t in range(0, 5):
                arrx5[t] = arrx5[t] + obstacspeed
                if arrx5[t] > 1080:
                    arrx5[t] = 0
                blit(arrx5[t], arry[4], enemy1)
                k = 0
            for i in range(0, k + 50):
                distance.append(0)
            for k in range(0, 5):
                distance[k] = (iscollision(x, arrx1[k], y, arry[0]))
                distance[k + 5] = (iscollision(x, arrx2[k], y, arry[1]))
                distance[k + 10] = (iscollision(x, arrx3[k], y, arry[2]))
                distance[k + 15] = (iscollision(x, arrx4[k], y, arry[3]))
                distance[k + 20] = (iscollision(x, arrx5[k], y, arry[4]))
            var = distance[0]
            for i in range(1, 30):
                var = var or distance[i]
                if var == 1:
                    tree2 = 1
            if y > scorerev[0] and y < scorerev[1] and minim > y:
                score2 = score2 + 10
                minim = scorerev[1]
            blitfix(fixedobsx[0], fixedobsy[0])
            blitfix(fixedobsx[11], fixedobsy[0])
            blitfix(fixedobsx[1], fixedobsy[1])
            blitfix(fixedobsx[10], fixedobsy[1])
            blitfix(fixedobsx[2], fixedobsy[2])
            blitfix(fixedobsx[9], fixedobsy[2])
            blitfix(fixedobsx[3], fixedobsy[3])
            blitfix(fixedobsx[8], fixedobsy[3])
            blitfix(fixedobsx[4], fixedobsy[4])
            blitfix(fixedobsx[7], fixedobsy[4])
            blitfix(fixedobsx[5], fixedobsy[5])
            blitfix(fixedobsx[3], fixedobsy[5])
            if y < scorerev[2] and y > scorerev[1] and minim < y:
                score2 = score2 + 10
                minim = scorerev[2]
            if y < scorerev[3] and y > scorerev[2] and minim < y:
                score2 = score2 + 5
                minim = scorerev[3]
            if y < scorerev[4] and y > scorerev[3] and minim < y:
                score2 = score2 + 10
                minim = scorerev[4]
            if y < scorerev[5] and y > scorerev[4] and minim < y:
                score2 = score2 + 5
                minim = scorerev[5]
            if y < scorerev[6] and y > scorerev[5] and minim < y:
                score2 = score2 + 10
                minim = scorerev[6]
            if y < scorerev[7] and y > scorerev[6] and minim < y:
                score2 = score2 + 5
                minim = scorerev[7]
            if y < scorerev[8] and y > scorerev[7] and minim < y:
                score2 = score2 + 10
                minim = scorerev[8]
            if y < 700 and y > scorerev[8] and minim < y:
                score2 = score2 + 5
                minim = 700
            if y > 700 and y < 780 and minim < y:
                score2 = score2 + 10
                minim = 0
                flag1 = 1
                flag2 = 0
                obstacspeed = obstacspeed + 10
                level = level + 1
                flagmessage = 1000
                messageplayer2end = 1
                pygame.display
                c1 = 1
                score = 0
                pygame.display.update()
                # print("Reached")
            text1 = font.render('score: ' + str(score), 1, (0, 0, 0))
            text2 = font.render('score2: ' + str(score2), 1, (0, 0, 0))
            text3 = font.render('level: ' + str(level), 1, (0, 0, 0))
            printf1(text1)
            printf2(text2)
            printf3(text3)
            if tree2 == 1 or tree3 == 1:
                flag1 = 1
                flag2 = 0
                c1 = 1
                obstacspeed = 20
                level = 1
                score = 0
                score2 = 0
                messageplayer2 = 1
                flagmessage = 1000
                seconds2 = 0
                seconds1 = 0

        pygame.display.update()
pygame.quit()
